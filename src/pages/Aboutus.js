import React from 'react'

function Aboutus() {
  return (
    <div>
      <h1>StudyHubStore</h1>
      <p>
        
       <h3> Study Hub Booths create enclosed spaces for concentration, minimising distractions, making them ideal for educational use, or focused working. They provide a flexible and cost effective choice for schools,
       libraries, call centres and commercial offices.
       </h3>
     
       <h3>Study Hub can be extended as a single sided or double sided configuration, by the addition of individual extension modules, and the range is available in a choice of six finishes.

Pinnable fabric panels in a variety of colours, are also available, which mount quickly and easily to the rear of the booth, to enable users to keep important notes or documents within easy reach.


</h3>
<h3>If required, a power module can be installed in each position, to provide power or charging facilities for the users portable devices.

Optional under desk cable management and CPU holders can also be specified, to keep the floor below the Study Hub free of cables and obstructions.

</h3>
        
      </p>
    </div>
  )
}

export default Aboutus
