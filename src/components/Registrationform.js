
import React, {useState} from 'react'
import { Input, FormGroup, Label, Form, Col, Row, Button } from 'reactstrap'
import axios from 'axios'


function RegistrationForm() {

    let [formData,setFormData] = useState({
        email: '',
        password: '',
        address: '',
        address2: '',
        city: '',
        state: '',
        zip: '',
        check: ''
    })

    const handleOnChange = (e) => {

        let { name, value, type, checked } = e.target;
        const inputValue = type === 'checkbox' ? checked : value;

        setFormData({
            ...formData,
            [name]: inputValue
        })

    }

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(formData)

        let register = async () => {
            try {let responce=await axios.get("",formData)
            }
            catch (err) {
                console.log(err)
            }
        }
        register()
    }

 return (

        <div style={{ border: '5px solid black', width: '500px', padding: '1rem', margin: '3rem auto', borderRadius: '10px' }}>

            <Form>
                <Row>
                    <Col md={6}>
                        <FormGroup>
                            <Label for="exampleEmail">Email</Label>
                            <Input
                                id="exampleEmail"
                                name="email"
                                placeholder="Enter a Email Id"
                                type="email"
                                value={formData.email}
                                onChange={handleOnChange}

                            />
                        </FormGroup>
                    </Col>
                    <Col md={6}>
                        <FormGroup>
                            <Label for="examplePassword"> Password</Label>
                            <Input
                                id="examplePassword"
                                name="password"
                                placeholder="Enter a Strong password"
                                type="password"
                                value={formData.password}
                                onChange={handleOnChange}

                            />
                        </FormGroup>
                    </Col>
                </Row>
                <FormGroup>
                    <Label for="exampleAddress">Address</Label>
                    <Input
                        id="exampleAddress"
                        name="address"
                        placeholder="House Number"
                        value={formData.address}
                        onChange={handleOnChange}

                    />
                </FormGroup>
                <FormGroup>
                    <Label for="exampleAddress2">
                        Address 2
                    </Label>
                    <Input
                        id="exampleAddress2"
                        name="address2"
                        placeholder="Apartment, studio, or floor"
                        value={formData.address2}
                        onChange={handleOnChange}
                    />
                </FormGroup>
                <Row>
                    <Col md={6}>
                        <FormGroup>
                            <Label for="exampleCity">City </Label>
                            <Input id="exampleCity" name="city" value={formData.city}
                        onChange={handleOnChange}/>
                        </FormGroup>
                    </Col>
                    <Col md={4}>
                        <FormGroup>
                            <Label for="exampleState"> State </Label>
                            <Input id="exampleState" name="state" value={formData.state}
                        onChange={handleOnChange}/>
                        </FormGroup>
                    </Col>
                    <Col md={2}>
                        <FormGroup>
                            <Label for="exampleZip"> Zip </Label>
                            <Input id="exampleZip" name="zip" value={formData.zip}
                        onChange={handleOnChange}/>
                        </FormGroup>
                    </Col>
                </Row>
                <FormGroup check>
                    <Input id="exampleCheck" name="check" type="checkbox" value={formData.check}
                        onChange={handleOnChange}/>
                    <Label checkfor="exampleCheck" > Check me out </Label>
                </FormGroup>
                <Button onClick={handleSubmit} type='submit'>Register</Button>
            </Form>
        </div>
    )
}

export default RegistrationForm

