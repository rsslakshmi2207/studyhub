import React from 'react'
import {Link} from "react-router-dom"

function Navigation() {

    const navstyles={
        backgroundColor:'aqua',
        color:'white',
        cursor:'pointer',
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between'
        
    }
    const Links={
        listStyleType:'none',
        color:'white',
        cursor:'pointer'
       
        

    }
  return (
    <div style={navstyles} >
      <h1> StudyHub.Store </h1>
    <div>
      <ul className='d-flex m-2'>
        <li style={Links} className='p-1'><Link to="/" style={{textDecoration:'none'}}>Homepage</Link></li>
        <li style={Links} className='p-1'><Link to="/Aboutus" style={{textDecoration:'none'}}>Aboutus</Link></li>
        <li style={Links} className='p-1'><Link to="/Contactus" style={{textDecoration:'none'}}>Contactus</Link></li>
        <li style={Links} className='p-1'><Link to="/RegistrationForm" style={{textDecoration:'none'}}>RegistrationForm</Link></li>
        <li style={Links} className='p-1'><Link to="/Login" style={{textDecoration:'none'}}>Login</Link></li>


      </ul>
    </div>
      
      </div>

      
  );
}

export default Navigation
