import React, {useState} from 'react'
import { Input, FormGroup, Label, Form, Button} from 'reactstrap'


function Login() {
    let [formData,setFormData] = useState({
        email: '',
        password: ''
    
    })

    const handleOnChange = (e) => {

        let { name, value, type, checked } = e.target;
        const inputValue = type === 'checkbox' ? checked : value;

        setFormData({
            ...formData,
            [name]: inputValue
        })
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(formData)

        let register = async () => {
            try {
            }
            catch (err) {
            }
        }
        register()
    }

  return (
    <div style={{ border: '5px solid black', width: '500px', padding: '1rem', margin: '3rem auto', borderRadius: '10px' }}>

<Form>
    <FormGroup>
          <center><h2>Login Page</h2></center>
    <Label for="exampleEmail">Email</Label>
      <Input
        id="exampleEmail"
        name="email"
        placeholder=" Enter your Email Id"
        type="email"
        value={formData.email}
        onChange={handleOnChange}

      />
    
    </FormGroup>
    
    <FormGroup >
    <Label for="examplePassword">
        Password
      </Label>
      <Input
        id="examplePassword"
        name="password"
        placeholder="Password"
        type="password"
        value={formData.password}
        onChange={handleOnChange}

      />
     
    </FormGroup>
    {' '}
   

    <Button onClick={handleSubmit} type='submit'> Submit</Button>
    <span><a href=" ">forget Password?</a></span>
  </Form>

    </div>
  )
}

export default Login
