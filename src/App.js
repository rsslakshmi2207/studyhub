import React from 'react'
import Navigation from './components/Navigation'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Homepage from './pages/Homepage';
import Aboutus from './pages/Aboutus';
import Contactus from './pages/Contactus';
import RegistrationForm from './components/Registrationform';
import Login from './components/Login';

function App() {
  return (
    <div>
      
       <BrowserRouter>
        <Navigation />
      <Routes>
        <Route path="/" element={<Homepage />}/>
        <Route path="/Aboutus" element={<Aboutus />}/>
        <Route path="/Contactus" element={<Contactus />}/>
        <Route path="/RegistrationForm" element={<RegistrationForm />}/>
        <Route path="/Login" element={<Login />}/>
      </Routes>
      
    </BrowserRouter>
  </div>
    
  );
}

export default App;

